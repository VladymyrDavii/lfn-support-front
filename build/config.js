const config = {
  url: 'https://support.liefernetzwerk.de/api',
  urlOrder: 'http://lfn.rexit.info/de/support/api',
  transport: 'web',
  redirectTo: 'http://lfn.rexit.info/support/auth?redirect=' + window.location.origin + '/auth/%3Ctoken%3E'
};

window.getConfig = config;