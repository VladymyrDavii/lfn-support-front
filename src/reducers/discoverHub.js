import {
  DISCOVER_HUB,
  SUCCESS
} from "../constants";

const initialState = null;

export default (state = initialState, action) => {
  const {type, response} = action;

  switch (type) {
    case DISCOVER_HUB + SUCCESS:
      return response.data;
    default:
      return state;
  }
}