import {
  SHOW_ORDER_INFO,
  SUCCESS
} from "../constants";

const initialState = {
  isShowOrderInfo: true
};

export default (state = initialState, action) => {
  const {type, payload} = action;

  switch (type) {
    case SHOW_ORDER_INFO:
      return {...state, isShowOrderInfo: payload};
    default:
      return state;
  }
}