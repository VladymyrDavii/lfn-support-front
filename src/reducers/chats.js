import {
  GET_CHATS,
  GET_OPEN_CHATS,
  SET_CURRENT_CHAT,
  START,
  SUCCESS,
  CREATE_NEW_CHAT,
  CLOSE_CHAT,
  ASSIGN_TO_CHAT, SHOW_LAST_MESSAGE, FAIL, GET_ORDER_INFORMATION
} from "../constants";

const initialState = {
  currentChat: null,
  currentChatData: null,

  chats: null,
  loadingChats: false,
  loadedChats: false,

  openChats: null,
  loadingOpenChats: false,
  loadedOpenChats: false,

  errorCreateChat: null,
  errorAssignToChat: null,

  orderInformation: null,
  loadingOrderInformation: false,
  loadedOrderInformation: false
};

export default (state = initialState, action) => {
  const {type, response, payload, errorResponse} = action;

  switch (type) {
    case SHOW_LAST_MESSAGE:
      const findChats = state.chats.data.filter(item => item.id === payload.data.chat_id);
      const msgAttachment = {
        data: {
          mime: payload.data.attachment && payload.data.attachment.data.mime,
          name: payload.data.attachment && payload.data.attachment.data.name,
          url: payload.data.attachment && payload.data.attachment.data.url
        }
      }
      if(findChats.length) {
        return {
          ...state,
            chats: {
              ...state.chats,
              data: state.chats.data.map(item => item.id === payload.data.chat_id ? (
                {
                  ...item,
                  lastMessage: {
                    data: {
                      text: payload.data.text,
                      created_at: payload.data.created_at,
                      type: payload.data.type,
                      attachment: payload.data.attachment ? msgAttachment : null
                    }
                  }
                }
              ) : (
                item
              )),
            }
          }
      }
      return {
        ...state,
        openChats: {
          ...state.openChats,
          data: state.openChats.data.map(item => item.id === payload.data.chat_id ? (
            {
              ...item,
              lastMessage: {
                data: {
                  text: payload.data.text,
                  created_at: payload.data.created_at,
                  type: payload.data.type,
                  attachment: payload.data.attachment ? msgAttachment : null
                }
              }
            }
          ) : (
            item
          )),
        }
      };

    case CREATE_NEW_CHAT + FAIL:
      const errorMsg = errorResponse.data.errors.detail;
      return {...state, errorCreateChat: errorMsg};

    case CREATE_NEW_CHAT:
      return {
        ...state,
        errorCreateChat: null,
        currentChat: undefined,
        currentChatData: undefined,
        openChats: {
          ...state.openChats,
          data: [
            ...state.openChats.data,
            {
              id: payload.data.chat_id,
              identity: payload.data.identity,
              is_closed: false,
              created_at: payload.data.occurred_on,
              operator: null,
              lastMessage: null
            }
          ]
        }
      };

    case CLOSE_CHAT:
      const chatLen = state.chats.data.length;
      const isOpenChats = state.openChats.data.filter(item => item.id !== action.payload.chat_id);
      const isChats = state.chats.data.filter(item => item.id !== action.payload.chat_id);
      history.pushState({}, null, window.location.origin + '/chatId/');
      if(isChats.length !== chatLen) {
        return {
          ...state,
          currentChat: undefined,
          currentChatData: undefined,
          chats: {
            ...state.chats,
            data: isChats
          }
        };
      }
      return {
        ...state,
        currentChat: undefined,
        currentChatData: undefined,
        openChats: {
          ...state.openChats,
          data: isOpenChats
        }
      };

    case ASSIGN_TO_CHAT + FAIL:
      const errorMsgAssign = errorResponse.data.errors.detail;
      return {...state, errorAssignToChat: errorMsgAssign};

    case ASSIGN_TO_CHAT:
      const openChatItem = state.openChats.data.filter(item => item.id === action.payload.chat_id);
      const newOpenChatItems = state.openChats.data.filter(item => item.id !== action.payload.chat_id);
      const chatItem = {
        id: payload.chat_id,
        identity: openChatItem[0].identity,
        is_closed: openChatItem[0].is_closed,
        created_at: payload.occurred_on,
        operator: payload.operator_identity,
        lastMessage: openChatItem[0].lastMessage
      };
      history.pushState({}, null, window.location.origin + '/chatId/' + payload.chat_id);
      return {
        ...state,
        currentChat: payload.chat_id,
        currentChatData: chatItem,
        openChats: {
          ...state.openChats,
          data: newOpenChatItems
        },
        chats: {
          ...state.chats,
          data: [
            ...state.chats.data,
            chatItem
          ]
        }
      };

    case SET_CURRENT_CHAT:
      const currentChat = state.chats && state.chats.data.filter(item => item.id === payload);
      const currentOpenChat = state.openChats && state.openChats.data.filter(item => item.id === payload);
      const filterChat = (currentChat && currentChat[0]) || (currentOpenChat && currentOpenChat[0]);
      return {...state, currentChat: payload, currentChatData: filterChat};

    case GET_CHATS + START:
      return {...state, loadedChats: false, loadingChats: true};

    case GET_CHATS + SUCCESS:
      const currentChatDataTemp = response.data && response.data.data.filter(item => item.id === state.currentChat);
      const isCurrentChatData = state.currentChatData ? state.currentChatData : currentChatDataTemp && currentChatDataTemp[0];
      return {...state, loadedChats: true, loadingChats: false, chats: response.data, currentChatData: isCurrentChatData};

    case GET_OPEN_CHATS + START:
      return {...state, loadedOpenChats: false, loadingOpenChats: true};

    case GET_OPEN_CHATS + SUCCESS:
      const currentOpenChatDataTemp = response.data && response.data.data.filter(item => item.id === state.currentChat);
      const isCurrentOpenChatData = state.currentChatData ? state.currentChatData : currentOpenChatDataTemp && currentOpenChatDataTemp[0];
      return {...state, loadedOpenChats: true, loadingOpenChats: false, openChats: response.data, currentChatData: isCurrentOpenChatData};

    case GET_ORDER_INFORMATION + START:
      return {...state, loadedOrderInformation: false, loadingOrderInformation: true};

    case GET_ORDER_INFORMATION + SUCCESS:
      return {...state, loadedOrderInformation: true, loadingOrderInformation: false, orderInformation: response.data};

    default:
      return state;
  }
}