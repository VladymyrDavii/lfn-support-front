import {
  GET_LIST_OF_MESSAGES, SEND_MESSAGE, CLOSE_MESSAGE,
  START,
  SUCCESS, CREATE_NEW_CHAT, FAIL
} from "../constants";

import {groupMessagesByDate} from "../utils";

const initialState = {
  messages: null,
  groupedMessages: null,
  loading: false,
  loaded: false,
  isGet: false,
  errorSendMsg: null,
  loadedAllDataMsg: true,
  offset: 0
};

export default (state = initialState, action) => {
  const {type, response, payload, errorResponse} = action;

  switch (type) {
    case CLOSE_MESSAGE:
      return {...state, messages: [], groupedMessages: {}, errorSendMsg: null};

    case GET_LIST_OF_MESSAGES + START:
      return {...state, loaded: false, loading: true, isGet: true, loadedAllDataMsg: false};

    case GET_LIST_OF_MESSAGES + SUCCESS:
      const data = response.data.data;
      const total = response.data.total;
      const loadedAllData = data.length === 0 || data.length === total || data.length < action.count ? false : true;
      // console.log('---')
      // console.log(total, 'total')
      // console.log(data.length, 'data.length')
      // console.log(action.count, 'action.count')
      // console.log(loadedAllData)
      const getData = state.messages && action.current ? [...data, ...state.messages] : data;
      return {...state, offset: action.offset, loaded: true, loading: false, messages: getData, isGet: true, groupedMessages: groupMessagesByDate(getData), errorSendMsg: null, loadedAllDataMsg: loadedAllData};

    case SEND_MESSAGE + FAIL:
      const errorMsg = errorResponse.data.errors.detail;
      return {...state, errorSendMsg: errorMsg};

    case SEND_MESSAGE:
      const newMessagesAttachment = {
        data: {
          mime: payload.data.attachment && payload.data.attachment.data.mime,
          name: payload.data.attachment && payload.data.attachment.data.name,
          url: payload.data.attachment && payload.data.attachment.data.url
        }
      }
      const newMessages = [
        ...state.messages,
        {
          id: payload.data.id,
          text: payload.data.text,
          type: payload.data.type,
          read_status: 0,
          created_at: payload.data.created_at,
          chat_id: payload.data.chat_id,
          attachment: payload.data.attachment ? newMessagesAttachment : null
        }
      ];
      return {
        ...state,
        errorSendMsg: null,
        groupedMessages: groupMessagesByDate(newMessages),
        messages: newMessages
      };

    default:
      return state;
  }
}