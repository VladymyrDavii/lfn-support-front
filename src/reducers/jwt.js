import {
  PARSE_JWT
} from "../constants";

const initialState = null;

export default (state = initialState, action) => {
  const {type, decodedJwt} = action;

  switch (type) {
    case PARSE_JWT:
      return decodedJwt;
    default:
      return state;
  }
}