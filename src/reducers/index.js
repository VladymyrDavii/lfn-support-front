import {combineReducers} from 'redux';
import counterReducer from './counter.js';
import jwt from './jwt.js';
import chats from './chats.js';
import messages from './messages.js';
import discoverHub from './discoverHub.js';
import utils from './utils.js';

export default combineReducers({
  count: counterReducer,
  decodedJwt: jwt,
  chats: chats,
  messages: messages,
  discoverHub: discoverHub,
  utils: utils
})