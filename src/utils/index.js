import jwt_decode from 'jwt-decode';

export const randomId = function() {
  return (Date.now() + Math.random()).toString();
};

export const imageReg = (mime) => /[\/.](gif|jpg|jpeg|tiff|png)$/i.test(mime.trim());

export const keyJwtLocalstorage = 'userToken';

export const getJwtToken = () => {
  const jwtToken = localStorage.getItem(keyJwtLocalstorage);
  const decodedJwt = jwtToken ? jwt_decode(jwtToken) : '';
  const expToken = decodedJwt.exp;
  const validToken = isValidToken(expToken);
  if(validToken) {
    return jwtToken;
  } else {
    setJwtToken();
    refreshJwtToken();
  }
};

export const refreshJwtToken = () => {
  window.location.href = window.getConfig.redirectTo;
};

export const setJwtToken = () => {
  const url = window.location.href.split('auth/')[1];
  if(url) {
    localStorage.setItem(keyJwtLocalstorage, url);
    history.pushState(null, null, window.location.origin);
  }
};

export const isValidToken = (token) => {
  const cTs=Math.floor(Date.now() / 1000);
  return (token>=cTs);
};

export const setHourMinutes = (time) => {
  const date = new Date(time);
  const hour = date.getHours() < 10 ? '0' + date.getHours() : date.getHours();
  const minutes = date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes();
  return hour + ':' + minutes;
};

const monthNames = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
];

export const setFullYearMonthDay = (time) => {
  const date = new Date(time);
  const year = date.getFullYear();
  const month = monthNames[date.getMonth()];
  const day = date.getDate() < 10 ? '0' + date.getDate() : date.getDate();
  return month + ' ' + day + ', ' + year;
};

export const groupMessagesByDate = function(data) {
  let groups = {};
  data.forEach(function(item) {
    let date = item.created_at.split('T')[0];
    if (date in groups) {
      groups[date].push(item);
    } else {
      groups[date] = new Array(item);
    }
  });
  return groups;
};

export const sortChatByTimeLastMessage = data => data.sort((a,b) => new Date(b.lastMessage.data.created_at).getTime() - new Date(a.lastMessage.data.created_at).getTime());

export const sortByTimeLastMessage = data => data.sort((a,b) => new Date(a.created_at).getTime() - new Date(b.created_at).getTime());


