import { createStore, applyMiddleware } from 'redux';
import reducer from '../reducers'

import api from "../middlewares/api.js";
import jwt from "../middlewares/jwt.js";
import thunk from 'redux-thunk';
import logger from 'redux-logger'

const enhancer = applyMiddleware(
  thunk,
  api,
  jwt,
  logger
);

const store = createStore(
  reducer,
  enhancer
);

window.store = store;

export default store