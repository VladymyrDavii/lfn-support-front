import React, { Component } from 'react';
import { connect } from "react-redux";
import { getListOfMessages } from "../../actionCreators";
import { Loader } from "../Loader/Loader";
import { getJwtToken } from "../../utils/index.js";
import ModalImage from "react-modal-image";
import ReactTextFormat from 'react-text-format';
import { setHourMinutes, setFullYearMonthDay, imageReg, sortByTimeLastMessage, addBlankTargets } from "../../utils";

class MessagesList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      offset: 0,
      count: 50,
      loadedScroll: false
    }
  }

  scrollBottom = () => {
    var elToScroll = document.querySelector(".chat__body-inner-scroll");
    if(elToScroll) {
      setTimeout(() => elToScroll.scrollIntoView(), 100);
    }
  };

  renderListMessages = () => {
    const {groupedMessages, currentChatData, loaded, isGet} = this.props;

    if(!loaded && isGet) return <Loader/>;
    if(!groupedMessages) return 'Have no one message';
    if(!currentChatData) return 'Select chat';
    if(!groupedMessages) return 'Select chat';

    const nameClient = currentChatData && currentChatData.identity;
    const sortedMessages = Object.keys(groupedMessages).sort();
    if(!sortedMessages.length) return 'Have no one message';

    return sortedMessages.map(function(key) {
      const dateMessage = setFullYearMonthDay(key);
      return sortByTimeLastMessage(groupedMessages[key]).map((messageItem, index) => <div className="chat__body-wrap" key={messageItem.id}>
        { index === 0 ? (<time className="chat__body-date">{dateMessage}</time>) : ('') }
          <div className="chat__message-item">
            { messageItem.type ? (
              <div className="chat__message-avatar chat__message-avatar--operator">o</div>
            ) : (
              <div className="chat__message-avatar">c</div>
            )}
            <div className="chat__message-info">
              { messageItem.type ? (
                <div className="chat__message-name">Operator</div>
              ) : (
                <div className="chat__message-name">{nameClient}</div>
              )}
              {messageItem.text || !messageItem.attachment ? (
                <div className="chat__message-text">
                  <ReactTextFormat
                    linkTarget={'_blank'}
                  >
                    {messageItem.text}
                  </ReactTextFormat>
                </div>
              ) : (messageItem.attachment && imageReg(messageItem.attachment.data.mime)) ? (
                <div className="chat__message-text">
                  <div className="chat__message-img">
                    <ModalImage
                      small={messageItem.attachment.data.url}
                      medium={messageItem.attachment.data.url}
                      large={messageItem.attachment.data.url}
                      alt={messageItem.attachment.data.name}
                      showRotate={true}
                    />
                  </div>
                </div>
              ) : (
                <div className="chat__message-text">
                  <a href={messageItem.attachment && messageItem.attachment.data.url} download target="_blank"><b>Click to download the file</b> - {messageItem.attachment && messageItem.attachment.data.name}</a>
                </div>
              )}
            </div>
            <time className="chat__message-time">{setHourMinutes(messageItem.created_at)}</time>
          </div>
      </div>);
    });
  };

  handleScroll = () => {
    const {currentChat, getListOfMessages, loaded, loadedAllDataMsg, offset} = this.props;
    const {count} = this.state;
    if(this.scroller && this.scroller.scrollTop <= 0 && loaded && loadedAllDataMsg) {
      const jwtToken = getJwtToken();
      getListOfMessages(jwtToken, currentChat, count, offset + count, true);
      this.setState({offset: offset + count});
      const heightBeforeRender = this.scroller.scrollHeight;
      setTimeout(() => {
        this.scroller.scrollTop = this.scroller.scrollHeight - heightBeforeRender + 80;
      }, 300);
    }
  };

  render() {
    const {renderListMessages, scrollBottom, handleScroll} = this;

    return (
      <div
        className="chat__body-inner"
        onScroll={handleScroll}
        ref={scroller => {
          this.scroller = scroller;
        }
      }>
        {renderListMessages()}
        <div className="chat__body-inner-scroll"> </div>
        <div>
          {scrollBottom()}
        </div>
      </div>
    );
  }
}

export default connect((state) => {
  return {
    offset: state.messages.offset,
    loading: state.messages.loading,
    loaded: state.messages.loaded,
    listMessages: state.messages.messages,
    loadedAllDataMsg: state.messages.loadedAllDataMsg,
    isGet: state.messages.isGet,
    groupedMessages: state.messages.groupedMessages,
    currentChatData: state.chats.currentChatData,
    currentChat: state.chats.currentChat
  }
}, {getListOfMessages})(MessagesList);