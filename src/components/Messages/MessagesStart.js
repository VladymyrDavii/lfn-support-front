import React, {Component} from 'react';
import {connect} from "react-redux";
import {assignToChat} from "../../actionCreators";
import {getJwtToken} from "../../utils";

class MessagesStart extends Component {

  handleAssignToChat = () => {
    const { assignToChat, currentChat } = this.props;
    const jwtToken = getJwtToken();
    if(currentChat) {
      assignToChat(jwtToken, currentChat);
    }
  };

  render() {
    const { handleAssignToChat } = this;
    const { error } = this.props;
    return (
      <div className="chat__btn-start-wrap">
        <button onClick={handleAssignToChat} type="button" className="chat__btn-start">Start Chat</button>
        {error ? <div className='error error--center'>{error}</div> : ''}
      </div>
    );
  }
}

export default connect((state) => {
  return {
    currentChat: state.chats.currentChat,
    error: state.chats.errorAssignToChat,
  }
}, {assignToChat})(MessagesStart);