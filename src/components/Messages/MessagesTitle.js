import React, { Component } from 'react';
import { connect } from "react-redux";
import {closeChat, showOrderInfo} from "../../actionCreators";
import {getJwtToken} from "../../utils";

class MessagesTitle extends Component {

  handleCloseChat = () => {
    const { currentChat, closeChat } = this.props;
    if(currentChat) {
      const jwtToken = getJwtToken();
      closeChat(jwtToken, currentChat);
    }
  };

  getTitle = () => {
    const { currentChatData } = this.props;
    if(currentChatData === undefined) return <div className="chat__name">Please select chat</div>;
    if(!currentChatData) return <div className="chat__name">Loading...</div>;
    return <div className="chat__name">
      <div className="chat__name-avatar">c</div>
      <div className="chat__name-text">{currentChatData.identity}</div>
    </div>;
  };

  handleCloseOrderInfo = () => {
    const {showOrderInfo} = this.props;
    showOrderInfo(true);
  };

  render() {
    const { getTitle, handleCloseChat, handleCloseOrderInfo } = this;
    const { currentChat, isShowOrderInfo } = this.props;
    return (
      <div className="chat__top">
        { getTitle() }
        <button type="button" className="chat__name-complete" onClick={handleCloseChat} disabled={!currentChat}>Complete</button>
        {!isShowOrderInfo ? (
          <button type="button" className="chat__name-complete chat__name-complete--show" onClick={handleCloseOrderInfo}>Show<br/>order</button>
        ) : (
          ''
        )}
      </div>
    );
  }
}

export default connect((state) => {
  return {
    currentChatData: state.chats.currentChatData,
    currentChat: state.chats.currentChat,
    isShowOrderInfo: state.utils.isShowOrderInfo
  }
}, { closeChat, showOrderInfo })(MessagesTitle);