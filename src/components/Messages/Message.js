import React, { Component } from 'react';
import MessagesTitle from "./MessagesTitle";
import MessagesList from "./MessagesList";
import MessagesForm from "./MessageForm";
import MessagesStart from "./MessagesStart";
import './messages.css';
import {connect} from "react-redux";
import {sendFile, sendMessage} from "../../actionCreators";

class Message extends Component {
  render() {
    const {currentChatData} = this.props;
    return (
      <div className="chat__main-wrap">
        <MessagesTitle/>
        <div className="chat__body">
          <MessagesList/>
          { currentChatData && currentChatData.operator ? (
            <MessagesForm/>
          ) : (
            <MessagesStart/>
          )}
        </div>
      </div>
    );
  }
}

export default connect((state) => {
  return {
    currentChatData: state.chats.currentChatData
  }
}, {sendMessage, sendFile})(Message);