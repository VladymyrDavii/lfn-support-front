import React, {Component} from 'react';
import { connect } from "react-redux";
import {sendMessage, sendFile} from "../../actionCreators";
import {getJwtToken} from "../../utils/index";

class MessagesForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      message: '',
      file: null
    };
  }

  handleChangeMessage = (e) => {
    this.setState({message: e.target.value});
  };

  handleSendFile = (e) => {
    const { currentChat } = this.props;
    const formData = new FormData();
    let files = e.target.files[0];
    formData.append('file', files);
    formData.append('chatId', currentChat);
    this.setState({file: formData});
  };

  handleSend = () => {
    const { currentChat, sendMessage, sendFile } = this.props;
    const { message, file } = this.state;

    const jwtToken = getJwtToken();

    if(message && file) {
      sendMessage(jwtToken, currentChat, message);
      sendFile(jwtToken, file);
      this.setState({message: ''});
      this.setState({file: null});
      return true;
    }

    if(message) {
      sendMessage(jwtToken, currentChat, message);
      this.setState({message: ''});
    }

    if(file) {
      sendFile(jwtToken, file);
      this.setState({file: null});
    }
  };

  onKeyPressed = (e) => {
    const { handleSend } = this;
    if(e.keyCode === 13){
      e.preventDefault();
      handleSend();
    }
  };

  render() {
    const { handleSend, handleChangeMessage, handleSendFile, onKeyPressed } = this;
    const { message, file } = this.state;
    const { error } = this.props;
    return (
      <div className="chat__send">
        <div className="chat__send-wrap">
          <label className="chat__send-file">
            <input id="#chat__send-file" type="file" onChange={handleSendFile}/>
            <svg xmlns="http://www.w3.org/2000/svg" width="37" height="39" fill="none">
              <path
                d="M35.52 18.338L19.437 34.42A10.506 10.506 0 114.58 19.563L20.662 3.48a7.004 7.004 0 019.905 9.905l-16.1 16.083a3.502 3.502 0 01-4.952-4.953l14.857-14.84"
                stroke="#559100"/>
            </svg>
            {/*{ file ? 'file uploaded' : '123' }*/}
          </label>
          <textarea value={ message } className="chat__send-field" placeholder="Write a message..." onChange={handleChangeMessage} onKeyDown={onKeyPressed} onKeyPress={onKeyPressed} onKeyUp={onKeyPressed} />
          <button disabled={!message.trim() && !file} onKeyDown={onKeyPressed} onKeyPress={onKeyPressed} onKeyUp={onKeyPressed} onClick={handleSend} type="submit" className="chat__send-btn">
            <svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M6.66675 16H25.3334" stroke="white"/>
              <path d="M16 6.66675L25.3333 16.0001L16 25.3334" stroke="white"/>
            </svg>
          </button>
        </div>
        {error ? <div className='error error--right' style={{position: 'absolute'}}>{error}</div> : ''}
      </div>
    );
  }
}

export default connect((state) => {
  return {
    currentChatData: state.chats.currentChatData,
    currentChat: state.chats.currentChat,
    error: state.messages.errorSendMsg
  }
}, {sendMessage, sendFile})(MessagesForm)