import React from 'react';
import './loader.css';

export const Loader = () => {
  return (
    <div className="loader lds-ring">
      <div></div>
      <div></div>
      <div></div>
      <div></div>
    </div>
  )
};