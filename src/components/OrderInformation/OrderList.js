import React, { Component } from 'react';
import {getJwtToken, imageReg, setFullYearMonthDay, setHourMinutes} from "../../utils";
import {getOrderInformation} from "../../actionCreators";
import {connect} from "react-redux";
import {Loader} from "../Loader/Loader";

class OrderList extends Component {

  renderListMessages = () => {
    const {loadedOrderInformation, orderInformation} = this.props;

    if(!orderInformation) return 'Select chat';
    if(!loadedOrderInformation) return <Loader/>;
    if(!orderInformation.length) return 'No orders';

    return orderInformation.map(orderItem => <ul key={orderItem.id} className="chat__order-list">
      <li className="chat__order-item">
        <div className="chat__order-item-left">Number:</div>
        <div className="chat__order-item-right">{orderItem.order_number}</div>
      </li>
      <li className="chat__order-item">
        <div className="chat__order-item-left">Link:</div>
        <div className="chat__order-item-right">
          <a href={orderItem.url} target="_blank">{orderItem.url}</a>
        </div>
      </li>
      {/*<li className="chat__order-item">*/}
      {/*  <div className="chat__order-item-left">Date:</div>*/}
      {/*  <div className="chat__order-item-right">{setFullYearMonthDay(orderItem.created_at)}</div>*/}
      {/*</li>*/}
      <li className="chat__order-item">
        <div className="chat__order-item-left">Shop:</div>
        <div className="chat__order-item-right">{orderItem.client}</div>
      </li>
      <li className="chat__order-item">
        <div className="chat__order-item-left">Client Name:</div>
        <div className="chat__order-item-right">{orderItem.client_name}</div>
      </li>
    </ul>);
  };

  render() {
    const {renderListMessages} = this;
    return (
      <div className="chat__order-list-wrap">
        <div className="chat__order-info">
          {renderListMessages()}
        </div>
      </div>
    );
  }
}

export default connect((state) => {
  return {
    orderInformation: state.chats.orderInformation,
    loadedOrderInformation: state.chats.loadedOrderInformation
  }
}, {getOrderInformation})(OrderList);
