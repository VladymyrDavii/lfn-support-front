import React from 'react';
import {connect} from "react-redux";
import {showOrderInfo} from "../../actionCreators";

function OrderTitle({showOrderInfo}) {

  const handleShowOrderInfo = () => {
    showOrderInfo(false);
  };

  const title = 'Order Information';
  return (<div className="chat__order-top">
    <h2 className="chat__order-title">{title}</h2>
    <button type="button" className="chat__order-close" onClick={handleShowOrderInfo}/>
  </div>)
}

export default connect(null, {showOrderInfo})(OrderTitle);