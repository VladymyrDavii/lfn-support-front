import React, { Component } from 'react';
import OrderTitle from "./OrderTitle";
import OrderList from "./OrderList";

import './order.css';

class Order extends Component {
  render() {
    return (
      <div className="chat__order">
        <div>
          <OrderTitle/>
          <OrderList/>
        </div>
      </div>
    );
  }
}

export default Order;