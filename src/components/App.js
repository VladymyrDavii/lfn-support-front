import React, { Component } from 'react';
import Auth from '../routes/Auth.js';
import {Route, Switch} from 'react-router-dom';
import ChatItem from '../routes/ChatItem.js';
import './app.css';

class App extends Component {
  render() {
    return (
      <div style={{height: '100%'}}>
        <Auth/>
        <Switch>
          <Route path="/chatId" component={ChatItem}/>
        </Switch>
      </div>
    );
  }
}

export default App