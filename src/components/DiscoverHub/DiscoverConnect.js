import React, { Component } from 'react';
import {connect} from "react-redux";
import {createNewChatEvent, closeChatEvent, operatorAssignedEvent, sendMessageEvent, showLastMessage, closeMessagesEvent} from "../../actionCreators";
import { EventSourcePolyfill } from 'event-source-polyfill';

class DiscoverConnect extends Component {

  componentDidMount() {
    const {data} = this.props;
    const {connect} = this;
    if(data) {
      connect(data.url, data.token)
    }
  }

  connect = (url, token) => {
    const {currentChat, closeMessagesEvent, createNewChatEvent, closeChatEvent, operatorAssignedEvent, sendMessageEvent, showLastMessage} = this.props;
    url = new URL(url);
    url.searchParams.append('topic', 'chats');
    url.searchParams.append('topic', 'messages');
    const es = new EventSourcePolyfill(url, {
      headers: {
        'Authorization': 'Bearer ' + token,
      }
    });

    const listener = function (event) {
      const type = event.type;
      const data = event.data;
      const parsedData = data && JSON.parse(data);

      if(type === 'message') {
        switch(parsedData.event) {
          case 'chat_created':
            createNewChatEvent(parsedData.payload);
            closeMessagesEvent(parsedData.payload);
            break;
          case 'chat_closed':
            closeChatEvent(parsedData.payload);
            closeMessagesEvent(parsedData.payload);
            break;
          case 'operator_assigned':
            operatorAssignedEvent(parsedData.payload);
            break;
          case 'new_message':
            if(currentChat && parsedData.payload.data.chat_id === window.store.getState().chats.currentChat) {
              sendMessageEvent(parsedData.payload);
            }
            showLastMessage(parsedData.payload);
            break;
          default:
            break;
        }
      }

    };
    es.addEventListener("open", listener);
    es.addEventListener("message", listener);
    es.addEventListener("error", listener);
  };

  render() {
    return (
      <div> </div>
    );
  }
}

export default connect((state) => {
  return {
    currentChat:state.chats.currentChat
  }
}, {
  createNewChatEvent,
  closeChatEvent,
  operatorAssignedEvent,
  sendMessageEvent,
  showLastMessage,
  closeMessagesEvent
})(DiscoverConnect);