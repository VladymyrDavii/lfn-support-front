import React, { Component } from 'react';
import {connect} from "react-redux";
import {discoverHub} from "../../actionCreators";
import {getJwtToken} from "../../utils";
import DiscoverConnect from "./DiscoverConnect.js";

class DiscoverHub extends Component {

  componentDidMount() {
    const {discoverHub} = this.props;
    const jwtToken = getJwtToken();
    if(jwtToken) {
      discoverHub(jwtToken);
    }
  }

  render() {
    const {discoverHubData} = this.props;
    if(discoverHubData)
    return (
      <div>
        <DiscoverConnect data={discoverHubData} />
      </div>
    );
    return false
  }
}

export default connect((state) => {
  return {
    discoverHubData: state.discoverHub
  }
}, {discoverHub})(DiscoverHub)