import React, { Component } from 'react';

import Chat from './AllChats/Chat.js';
import Messages from './Messages/Message.js';
import Order from "./OrderInformation/Order.js";
import DiscoverHub from './DiscoverHub/DiscoverHub.js';
import { connect } from 'react-redux'
import {getOrderInformation, parseJwt} from "../actionCreators";

import {getJwtToken, keyJwtLocalstorage} from "../utils/index";

import './app.css';

class App extends Component {
  render() {
    const {parseJwt, isShowOrderInfo} = this.props;
    const jwtToken = getJwtToken();

    if(!jwtToken) {
      parseJwt();
    } else {
      // history.pushState({}, null, window.location.origin);
    }

    return (
      <div className="chat">
        <div className="chat__left">
          <Chat/>
        </div>
        <div className="chat__main">
          <Messages/>
        </div>
        {isShowOrderInfo ? (
          <div className="chat__right">
            <Order/>
          </div>
        ) : (
          ''
        )}
        <DiscoverHub/>
      </div>
    );
  }
}

export default connect((state) => {
  return {
    decodedJwt: state.decodedJwt,
    currentChat: state.chats.currentChat,
    isShowOrderInfo: state.utils.isShowOrderInfo
  }
}, {parseJwt, getOrderInformation})(App)