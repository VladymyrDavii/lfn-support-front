import React from 'react';

function ChatTitle() {
  const title = 'All Chats';
  return <div className="chat__title-all">{title}</div>
}

export default ChatTitle;