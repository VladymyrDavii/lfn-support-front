import React, { Component } from 'react';
import ChatTitle from "./ChatTitle";
import ChatUnassigned from "./ChatUnassigned";
import ChatAssigned from "./ChatAssigned";
import ChatFormCreate from "./ChatFormCreate";

import './chats.css';

class Message extends Component {
  render() {
    return (
      <div className="chat__left-wrap">
        <ChatTitle/>
        <ChatUnassigned/>
        <ChatAssigned/>
        <ChatFormCreate/>
      </div>
    );
  }
}

export default Message;