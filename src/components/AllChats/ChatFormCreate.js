import React, { Component } from 'react';
import { connect } from "react-redux";
import { createNewChat } from "../../actionCreators";
import {getJwtToken} from "../../utils/index";

class ChatFormCreate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      form: ''
    };
  }

  handleChange = (event) => {
    this.setState({form: event.target.value});
  };

  handleClick = () => {
    const transport = window.getConfig.transport;
    const {form} = this.state;
    const {createNewChat} = this.props;
    if(form) {
      const jwtToken = getJwtToken();
      createNewChat(jwtToken, transport, form);
      this.setState({form: ''});
    }
  };

  render() {
    const {handleClick, handleChange} = this;
    const {form} = this.state;
    const {error} = this.props;
    return (
      <div className="chat__create">
        <h2 className="chat__create-title g-chat-title">Renew / Create Chats</h2>
        <div className="chat__create-wrap">
          <div className="chat__create-field">
            <input type="text" value={form} onChange={handleChange}/>
          </div>
          <button onClick={handleClick} disabled={!form} className="chat__create-btn" type="button">Renew / Create</button>
          {error ? <div className='error error--right'>{error}</div> : ''}
        </div>
      </div>
    );
  }
}

export default connect((state) => {
  return {
    error: state.chats.errorCreateChat
  }
}, {createNewChat})(ChatFormCreate)