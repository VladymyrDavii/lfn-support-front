import React, { Component } from 'react';
import { connect } from "react-redux";
import { getChats } from "../../actionCreators";
import { getJwtToken, sortChatByTimeLastMessage } from "../../utils/index";
import { Loader } from "../Loader/Loader.js";
import { setHourMinutes } from "../../utils/index.js";

import {NavLink} from 'react-router-dom';

class ChatAssigned extends Component {
  componentDidMount() {
    const {getChats, loadingChats, loadedChats} = this.props;
    const jwtToken = getJwtToken();
    if(!loadedChats && !loadingChats) {
      getChats(jwtToken);
    }
  }

  renderListChats = () => {
    const {listChats, loadedChats} = this.props;

    if(!loadedChats) return <Loader/>;
    if(!listChats) return <div className="chat__empty">Empty assigned chats</div>;
    if(!listChats.data.length) return <div className="chat__empty">Empty assigned chats</div>;

    const listChat = sortChatByTimeLastMessage(listChats.data);
    return listChat.map(chatItem => <li className="chat__assigned-item" key={chatItem.id}>
      <NavLink
        className="chat__assigned-link"
        to={`/chatId/${chatItem.id}`}
      >
        <div className="chat__assigned-avatar">c</div>
        <div className="chat__assigned-massage">
        <div className="chat__assigned-subtitle">{chatItem.identity}</div>
          {chatItem.lastMessage ? (
            <div className="chat__assigned-text">{chatItem.lastMessage && chatItem.lastMessage.data.text || chatItem.lastMessage.data.attachment && chatItem.lastMessage.data.attachment.data.name}</div>
          ) : (
            <div className="chat__assigned-text">No messages</div>
          )}
        </div>
        {chatItem.lastMessage ? (
          <time className="chat__assigned-time">{setHourMinutes(chatItem.lastMessage.data.created_at)}</time>
        ) : (
          ''
        )}
      </NavLink>
    </li>);
  };

  render() {
    const {renderListChats} = this;
    return (
      <div className="chat__assigned">
        <h2 className="chat__assigned-title g-chat-title">Assigned Chats</h2>
        <ul className="chat__assigned-list">
          {renderListChats()}
        </ul>
      </div>
    );
  }
}

export default connect((state) => {
  return {
    loadingChats: state.chats.loadingChats,
    loadedChats: state.chats.loadedChats,
    listChats: state.chats.chats
  }
}, {getChats})(ChatAssigned)
