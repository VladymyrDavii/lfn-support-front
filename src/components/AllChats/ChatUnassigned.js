import React, { Component } from 'react';
import { connect } from 'react-redux'
import { getOpenChats } from "../../actionCreators";
import {getJwtToken, setHourMinutes, sortChatByTimeLastMessage} from "../../utils/index";
import {Loader} from "../Loader/Loader";
import {NavLink} from "react-router-dom";

class ChatUnassigned extends Component {
  componentDidMount() {
    const {getOpenChats, loadingOpenChats, loadedOpenChats} = this.props;
    const jwtToken = getJwtToken();
    if(!loadedOpenChats && !loadingOpenChats) {
      getOpenChats(jwtToken);
    }
  }

  renderListOpenChats = () => {
    const {listOpenChats, loadedOpenChats} = this.props;

    if(!loadedOpenChats) return <Loader/>;
    if(!listOpenChats) return <div className="chat__empty">Empty assigned chats</div>;
    if(!listOpenChats.data.length) return <div className="chat__empty">Empty assigned chats</div>;

    const listChat = sortChatByTimeLastMessage(listOpenChats.data);
    return listChat.map(chatItem => <li className="chat__unassigned-item" key={chatItem.id}>
      <NavLink
        className="chat__unassigned-link"
        to={`/chatId/${chatItem.id}`}
      >
        <div className="chat__unassigned-avatar">c</div>
        <div className="chat__unassigned-massage">
        <div className="chat__unassigned-subtitle">{chatItem.identity}</div>
          {chatItem.lastMessage ? (
            <div className="chat__unassigned-text">{chatItem.lastMessage && chatItem.lastMessage.data.text || chatItem.lastMessage.data.attachment && chatItem.lastMessage.data.attachment.data.name}</div>
          ) : (
            <div className="chat__unassigned-text">No messages</div>
          )}
        </div>
        {chatItem.lastMessage ? (
          <time className="chat__unassigned-time">{setHourMinutes(chatItem.lastMessage.data.created_at)}</time>
        ) : (
          ''
        )}
      </NavLink>
    </li>);
  };

  render() {
    const {renderListOpenChats} = this;
    return (
      <div className="chat__unassigned">
        <h2 className="chat__unassigned-title g-chat-title">Unassigned Chats</h2>
        <ul className="chat__unassigned-list">
          {renderListOpenChats()}
        </ul>
      </div>
    );
  }
}

export default connect((state) => {
  return {
    loadingOpenChats: state.chats.loadingOpenChats,
    loadedOpenChats: state.chats.loadedOpenChats,
    listOpenChats: state.chats.openChats
  }
}, {getOpenChats})(ChatUnassigned);