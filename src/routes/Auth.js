import React, {Component} from 'react';
import {Route} from 'react-router-dom';
import ChatMain from "../components/ChatMain.js";

const redirect = window.getConfig.redirectTo;

import {getJwtToken} from "./../utils/index";

class Auth extends Component {
  render() {
    const url = window.location.href.split('auth/')[1];
    const jwtToken = getJwtToken();
    const checkJwtToken = jwtToken && jwtToken !== 'undefined';
    const isCheckJwtToken = checkJwtToken ? checkJwtToken : url;

    return (
      <div style={{height: '100%'}}>
        { !isCheckJwtToken ? (
          <Route path='/' component={() => {
            window.location.href = redirect;
            return null;
          }}/>
        ) : (
          <ChatMain/>
        )}
      </div>
    )
  }
}

export default Auth;