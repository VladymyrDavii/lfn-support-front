import React, {Component} from 'react';
import {Route} from 'react-router-dom';
import {connect} from "react-redux";
import {setCurrentChat, getListOfMessages, getOrderInformation} from "../actionCreators";
import {getJwtToken} from "../utils";

class ChatItem extends Component {
  render() {
    return (
      <div>
        <Route path="/chatId/:id" render={this.getArticle}/>
      </div>
    )
  }

  getArticle = ({match}) => {
    const {setCurrentChat, getListOfMessages, currentChatData, getOrderInformation} = this.props;
    const {id} = match.params;
    setCurrentChat(id);

    const jwtToken = getJwtToken();
    const identity = currentChatData && currentChatData.identity;
    if(currentChatData && currentChatData.id === id) {
      getListOfMessages(jwtToken, id, 50, 0, false);
      getOrderInformation(jwtToken, identity);
    }
  }
}

export default connect((state) => {
  return {
    currentChatData: state.chats.currentChatData
  }
}, {setCurrentChat, getListOfMessages, getOrderInformation})(ChatItem)