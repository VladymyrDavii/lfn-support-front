// import { EventSourcePolyfill } from 'event-source-polyfill';
// import {createNewChatEvent} from "../actionCreators";
// import {keyJwtLocalstorage} from "../utils";
//
// export default store => next => action => {
//   const {jwtToken, mercure, payload} = action;
//   if (!mercure) return next(action);
//
//   if(mercure) {
//     payload.url = new URL(payload.url);
//     payload.url .searchParams.append('topic', 'chats');
//     payload.url .searchParams.append('topic', 'messages');
//     const es = new EventSourcePolyfill(payload.url, {
//       headers: {
//         'Authorization': 'Bearer ' + jwtToken,
//       }
//     });
//
//     const listener = function (event) {
//       console.log('EVENT')
//       console.log(event)
//       const data = event;
//       const type = data.type;
//
//       const jwtToken = localStorage.getItem(keyJwtLocalstorage);
//       if(type === 'message' && jwtToken) {
//         console.log('---------------')
//         console.log('---------------')
//         console.log('---------------')
//         console.log('---------------')
//         switch (data.event) {
//           case 'chat_created':
//             createNewChatEvent(data.payload);
//             break;
//           case 'DECREMENT':
//             break;
//           default:
//             break;
//         }
//       }
//     };
//
//     es.addEventListener("open", listener);
//     es.addEventListener("message", listener);
//     es.addEventListener("error", listener);
//   }
// }
// // message: {"event":"chat_created","payload":{"occurred_on":"2020-02-06T11:50:19.014367+01:00","chat_id":"5dbf3e55-aead-4288-84e8-7b8dbd6afa55","identity":"666"}}