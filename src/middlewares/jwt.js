import jwt_decode from 'jwt-decode';
import {getJwtToken, refreshJwtToken, setJwtToken} from "../utils/index";

export default store => next => action => {
  if (!action.parseJwt) return next(action);

  const jwtToken = getJwtToken();
  if(!jwtToken) {
    const url = window.location.href.split('auth/')[1];
    const decodedJwt = jwt_decode(url);
    setJwtToken();
    refreshJwtToken();
    next({
      ...action,
      decodedJwt: decodedJwt
    })
  } else {
    const decodedJwt = jwt_decode(jwtToken);
    if(decodedJwt) {
      history.pushState({}, null, window.location.origin);
    }
    next({
      ...action,
      decodedJwt: decodedJwt
    })
  }
}