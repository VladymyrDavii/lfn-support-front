import axios from 'axios';
import {START, SUCCESS, FAIL, DELETE, POST, CREATE_NEW_CHAT} from "../constants";
import {refreshJwtToken, setJwtToken} from '../utils/index.js';

export default store => next => action => {
  const {callAPI, jwtToken, type, typeCallAPI, payload, formData, ...rest} = action;
  if (!callAPI) return next(action);

  next({
    ...rest, type: type + START
  });

    const config = {
      headers: {
        Authorization: `Bearer ${jwtToken}`,
        'content-type': formData ? 'multipart/form-data'  : 'application/json'
      }
    };
    if(typeCallAPI === POST) {
        axios.post(
          callAPI,
          payload,
          config
        )
        .then(function (response) {
          // handle success
          return next({...rest, type: type + SUCCESS, response})
        })
        .catch(function (error) {
          if(error.response.status === 401) {
            setJwtToken();
            refreshJwtToken();
          }
          // handle error
          const errorResponse = error.response;
          return next({...rest, type: type + FAIL, errorResponse});
        })
    } else if(typeCallAPI === DELETE) {
      axios.delete(
        callAPI,
        config
      )
        .then(function (response) {
          // handle success
          return next({...rest, type: type + SUCCESS, response})
        })
        .catch(function (error) {
          if(error.response.status === 401) {
            setJwtToken();
            refreshJwtToken();
          }
          // handle error
          return {...rest, type: type + FAIL, error};
        })
    } else {
      axios.get(
        callAPI,
        config
      )
        .then(function (response) {
          // handle success
          return next({...rest, type: type + SUCCESS, response})
        })
        .catch(function (error) {
          if(error.response.status === 401) {
            setJwtToken();
            refreshJwtToken();
          }
          // handle error
          return {...rest, type: type + FAIL, error};
        })
    }
}