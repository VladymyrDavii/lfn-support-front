export const START = '_START';
export const SUCCESS = '_SUCCESS';
export const FAIL = '_FAIL';

export const POST = 'POST';
export const DELETE = 'DELETE';

export const PARSE_JWT = 'PARSE_JWT';

export const GET_CHATS = 'GET_CHATS';
export const GET_OPEN_CHATS = 'GET_OPEN_CHATS';
export const SET_CURRENT_CHAT = 'SET_CURRENT_CHAT';
export const ASSIGN_TO_CHAT = 'ASSIGN_TO_CHAT';

export const GET_ORDER_INFORMATION = 'GET_ORDER_INFORMATION';
export const GET_LIST_OF_MESSAGES = 'GET_LIST_OF_MESSAGES';
export const SEND_MESSAGE = 'SEND_MESSAGE';
export const SEND_FILE = 'SEND_FILE';
export const SHOW_LAST_MESSAGE = 'SHOW_LAST_MESSAGE';
export const CLOSE_MESSAGE = 'CLOSE_MESSAGE';

export const CREATE_NEW_CHAT = 'CREATE_NEW_CHAT';
export const CLOSE_CHAT = 'CLOSE_CHAT';

export const DISCOVER_HUB = 'DISCOVER_HUB';

export const SHOW_ORDER_INFO = 'SHOW_ORDER_INFO';