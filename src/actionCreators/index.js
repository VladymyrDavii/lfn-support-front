import {
  PARSE_JWT,
  GET_CHATS,
  GET_OPEN_CHATS,
  SET_CURRENT_CHAT,
  GET_LIST_OF_MESSAGES,
  CREATE_NEW_CHAT,
  CLOSE_CHAT,
  SEND_MESSAGE,
  SEND_FILE,
  ASSIGN_TO_CHAT,
  DISCOVER_HUB,
  POST,
  DELETE,
  CLOSE_MESSAGE,
  SHOW_LAST_MESSAGE,
  GET_ORDER_INFORMATION, SHOW_ORDER_INFO
} from "../constants";

const apiUrl = window.getConfig.url;
const apiUrlOrder = window.getConfig.urlOrder;

export function parseJwt() {
  return {
    type: PARSE_JWT,
    parseJwt: true
  }
}

export function getChats(jwtToken) {
  return {
    type: GET_CHATS,
    callAPI: `${apiUrl}/my-chats?count=200`,
    jwtToken: jwtToken
  }
}

export function getOpenChats(jwtToken) {
  return {
    type: GET_OPEN_CHATS,
    callAPI: `${apiUrl}/open-chats`,
    jwtToken: jwtToken
  }
}

export function setCurrentChat(id) {
  return {
    type: SET_CURRENT_CHAT,
    payload: id
  }
}

export function getListOfMessages(jwtToken, chatId, count = 50, offset = 0, current) {
  return {
    type: GET_LIST_OF_MESSAGES,
    callAPI: `${apiUrl}/chat/${chatId}/messages?order=0&count=${count}&offset=${offset}`,
    jwtToken: jwtToken,
    offset: offset,
    current: current,
    count: count
  }
}

export function getOrderInformation(jwtToken, chatId) {
  return {
    type: GET_ORDER_INFORMATION,
    callAPI: `${apiUrlOrder}/phone?phone=${chatId}`,
    jwtToken: jwtToken
  }
}

export function createNewChat(jwtToken, transport, identity) {
  return {
    type: CREATE_NEW_CHAT,
    typeCallAPI: POST,
    callAPI: `${apiUrl}/create-chat`,
    payload: {transport, identity},
    jwtToken: jwtToken
  }
}

export function closeChat(jwtToken, chatId) {
  return {
    type: CLOSE_CHAT,
    typeCallAPI: DELETE,
    callAPI: `${apiUrl}/close-chat/${chatId}`,
    jwtToken: jwtToken
  }
}

export function sendMessage(jwtToken, chatId, message) {
  return {
    type: SEND_MESSAGE,
    typeCallAPI: POST,
    callAPI: `${apiUrl}/send-message`,
    payload: {message, chatId},
    jwtToken: jwtToken
  }
}

export function sendFile(jwtToken, formData) {
  return {
    type: SEND_FILE,
    typeCallAPI: POST,
    callAPI: `${apiUrl}/send-file`,
    payload: formData,
    formData: true,
    jwtToken: jwtToken
  }
}

export function assignToChat(jwtToken, chatId) {
  return {
    type: ASSIGN_TO_CHAT,
    typeCallAPI: POST,
    callAPI: `${apiUrl}/assign-to-chat/${chatId}`,
    payload: {chatId},
    jwtToken: jwtToken
  }
}

export function discoverHub(jwtToken) {
  return {
    type: DISCOVER_HUB,
    callAPI: `${apiUrl}/discover-hub`,
    jwtToken: jwtToken
  }
}

export function createNewChatEvent(data) {
  return {
    type: CREATE_NEW_CHAT,
    payload: {data}
  }
}

export function closeChatEvent(chatId) {
  return {
    type: CLOSE_CHAT,
    payload: chatId
  }
}

export function closeMessagesEvent(chatId) {
  return {
    type: CLOSE_MESSAGE,
    payload: chatId
  }
}

export function operatorAssignedEvent(data) {
  return {
    type: ASSIGN_TO_CHAT,
    payload: data
  }
}

export function sendMessageEvent(data) {
  return {
    type: SEND_MESSAGE,
    payload: data
  }
}

export function showLastMessage(data) {
  return {
    type: SHOW_LAST_MESSAGE,
    payload: data
  }
}

export function showOrderInfo(data) {
  return {
    type: SHOW_ORDER_INFO,
    payload: data
  }
}